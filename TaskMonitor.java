package com.xxx.aop;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * desc:定时任务监控注解
 *
 * @author wangchengwang
 * @date 2018/11/21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Component
public @interface TaskMonitor {
    /**
     * 定时任务名称
     *
     * @return String string
     */
    String taskName();
}
