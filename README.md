# TaskMonitor

#### 项目介绍
定时任务监控,在定时任务方法上加上注解(默认是项目名称.定时任务类名.定时任务方法名,可以在注解中定义),可以返回定时任务监控返回值TaskMonitorAopResponse,属性是是否成功,失败原因,成功备注,用于跑完后,自定义代码检测结果,比如查看跑批完成后,数据库有没有插入正确的条数等,在监控配置表输入监控任务名称,监控邮件接收人,在监控邮件发送方法,写入邮件发送方法,配置监控邮件发送的定时任务,在需要监控的项目中引入监控aop代码,便可以监控定时任务,包括定时任务未跑提醒,定时任务异常提醒,定时任务自定义结果,定时任务超时异常等

#### 软件架构
注解+Aop


#### 安装教程

1. 跑一下sql文件,参加2张表,分别是记录表和配置表
2. 在对应项目中引入监控代码,在管理项目中引入定时发送定时任务监控邮件的代码,并配置定时任务
3. 可以使用配置或者注解将监控代码注册到spring中,如:
 < bean id="taskMonitorAop" class="com.xxx.aop.TaskMonitorAop">
    <property name="dataSource" ref="dataSource"/>
  < /bean>
  < aop:aspectj-autoproxy expose-proxy="true"/>

#### 使用说明

1. 监控配置表需要配置定时任务名称和邮件接收人,需要使用mybatis自动代码生成的映射文件
2. 定时任务监控代码需要引入到对应项目
3. 建议将监控代码放在公共项目中,被其他项目引入,并配置

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)