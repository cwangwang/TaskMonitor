DROP TABLE IF EXISTS `task_monitor_config`;
CREATE TABLE `task_monitor_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_user` varchar(255) DEFAULT NULL COMMENT '任务告警邮件接收人',
  `task_name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `send_type` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL COMMENT '描述',
  `gmt_create` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` bigint(13) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `task_monitor_record`;
CREATE TABLE `task_monitor_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(255) DEFAULT NULL,
  `partner_id` varchar(255) DEFAULT NULL COMMENT '任务归属者',
  `hostname` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `failure_cause` varchar(5000) DEFAULT NULL,
  `is_success` int(11) DEFAULT '0' COMMENT '默认0,0失败，1成功',
  `exception_time` timestamp NULL DEFAULT NULL  CURRENT_TIMESTAMP,
  `start_time` timestamp NULL DEFAULT NULL  CURRENT_TIMESTAMP,
  `complete_time` timestamp NULL DEFAULT NULL  CURRENT_TIMESTAMP,
  `remarks` varchar(255) DEFAULT NULL,
  `gmt_create` bigint(13) DEFAULT NULL COMMENT '创建时间',
  `gmt_modified` bigint(13) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAU